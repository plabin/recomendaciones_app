from django.urls import path

from . import views

app_name = 'recomendaciones'  # Namespace
urlpatterns = [
    # the 'name' value as called by the {% url %} template tag
    path('', views.index, name='index'),
    path('enviadas/', views.RecomendacionesListView.as_view(), name='enviadas'),
    path('api/recomendaciones/', views.Recomendaciones.as_view(), name='lista'),
]