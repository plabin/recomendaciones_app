from django.test import TestCase
from django.utils import timezone
from django.urls import reverse

# Create your tests here.

class ViewAccessTest(TestCase):
    """
        Tests to check if you can open the views.
    """
    def test_accessToIndex(self):
        response = self.client.get(reverse('recomendaciones:index'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "GOTO Admin")

    def test_accessToAdmin(self):
        response = self.client.get(reverse('admin:index'))
        self.assertEqual(response.status_code, 302)
        self.assertContains(response, 'Django administration')
