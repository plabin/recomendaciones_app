from django.contrib import admin

from . import models

# Register your models here.
class RecomendacionAdmin(admin.ModelAdmin):
    pass

admin.site.register(models.Recomendacion, RecomendacionAdmin)