import json

from django.shortcuts import render, reverse, get_list_or_404
from django.http import HttpResponseRedirect
from django.utils import timezone
from django.views import generic

from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from .app_settings import app_settings
from . import models

# Create your views here.

def index(request):
    if request.method == 'GET':
        # Setup dark theme
        if not request.session.get('dark_theme'):
            request.session['dark_theme'] = app_settings.SESSION_DEFAULT_THEME_ISDARK
        if 'changetheme' in request.GET.keys():
            request.session['dark_theme'] = not request.session.get('dark_theme')
            return HttpResponseRedirect(reverse('recomendaciones:index'))
        ##

    if request.method == 'POST':
        print(request.POST)
        new_rec = models.Recomendacion(
            titulo=request.POST.get('titulo'),
            contenido=request.POST.get('contenido_receta'),
            pub_date=timezone.now()
            )   
        new_rec.save()

    return render(request, 'recomendaciones/index.html')


class RecomendacionesListView(generic.ListView):
    model = models.Recomendacion
    template_name = 'recomendaciones/enviadas.html'


# API
class Recomendaciones(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (IsAuthenticated,)

    def get(self, request, format=None):
        lista_rec = get_list_or_404(models.Recomendacion.objects.all())
        lista_rec = [x.as_json_dict() for x in lista_rec]
        print(lista_rec)

        content = {
            'recomendaciones': json.dumps(lista_rec)
        }
        return Response(content)