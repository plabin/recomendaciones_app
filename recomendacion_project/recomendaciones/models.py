from django.db import models
from django.utils import timezone

# Create your models here.

class Recomendacion(models.Model):
    titulo = models.CharField('Título', max_length=80)
    contenido = models.TextField('Contenido')
    pub_date = models.DateTimeField('Date Published', default=timezone.now)
    seleccionado = models.BooleanField(default=False)

    def __str__(self):
        return ' - '.join([self.titulo, str(self.pub_date)])

    def as_json_dict(self):
        json_dict = {
            'titulo': self.titulo,
            'contenido': self.contenido,
            'pub_date': str(self.pub_date),
            'seleccionado': self.seleccionado
        }
        return json_dict
